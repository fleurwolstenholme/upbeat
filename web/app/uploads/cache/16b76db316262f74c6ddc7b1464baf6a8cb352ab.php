<header id="siteHeader" class="site-header py-4 pb-xl-0">
  <div class="container pb-5">
    <nav class="navbar align-items-xl-start navbar-expand-xl p-0">
      <a class="navbar-brand p-0" href="/">
        <a class="navbar-brand align-self-center" href="<?php echo e(home_url('/')); ?>"> <img class="icon" src=" <?= App\asset_path('images/upbeatnutrition_finalhigh.png'); ?> " alt="Upbeat Nutrition"></a>
      </a>
      <button id="navBtn" class="navbar-toggler hamburger hamburger--vortex" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>

      <div class="collapse navbar-collapse pt-4 mt-1" id="primaryNav">
        <ul class="navbar-nav ml-auto">
          <?php $__currentLoopData = $primary_navigation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="px-4 h4 text-uppercase nav-item <?php echo e($item->classes ?? ''); ?> <?php echo e($item->active ? 'active' : ''); ?>">
              <a id="navLink<?php echo e($item->id); ?>" class="nav-link" href="<?php echo e($item->url); ?>"><?php echo e($item->label); ?></a>
            </li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
      </div>
    </nav>
  </div>
</header>
