<footer class="page-footer text-info bg-white">
  <div class="container">
    <hr>
    <div class="row pb-4 d-flex justify-content-center align-self-center">

      <div class="d-none d-lg-block col-lg-2">
        <a href="/"> <img class="w-75" src=" @asset('images/upbeatnutrition_finalhigh.png') " alt="Upbeat Nutrition Logo"></a>
      </div>

      <div class="col-sm-2 col-lg-2">
        <ul class="list-unstyled">
          <li><a href="/about">About</a></li>
          <li><a href="/services">Services</a></li>
        </ul>
      </div>
      <div class="col-sm-2 col-lg-2">
        <ul class="list-unstyled">
          <li><a href="/blog">Blog</a></li>
          <li><a href="/contact">Contact</a></li>
        </ul>
      </div>
      <div class="col-sm-2 col-lg-2">
        <ul class="list-unstyled">
          <li><a href="/tcs">T&Cs</a></li>
          <li><a href="/privacy">Privacy</a></li>
        </ul>
      </div>


      <div class="col-sm-12 col-lg-3 py-4 follow">
        <p class="text-primary">Follow Us!</p>
        <a class="" target="_blank" rel="noopener" href="{{ $social_media['facebook'] }}"><i class="fab fa-2x fa-facebook"></i></a>
        <a class="ml-2" target="_blank" rel="noopener" href="{{ $social_media['instagram'] }}"><i class="fab fa-2x fa-instagram"></i></a>
          <p><u><a href="https://www.upbeatnutrition.co.nz"></a></u><small> © 2020 Upbeat Nutrition</small></p>
      </div>
    </div>
    </div>
</footer>