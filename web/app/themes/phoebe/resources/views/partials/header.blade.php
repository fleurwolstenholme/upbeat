<header id="siteHeader" class="site-header py-4 pb-xl-0">
  <div class="container pb-5">
    <nav class="navbar align-items-xl-start navbar-expand-xl p-0">
      <a class="navbar-brand p-0" href="/">
        <a class="navbar-brand align-self-center" href="{{ home_url('/') }}"> <img class="icon" src=" @asset('images/upbeatnutrition_finalhigh.png') " alt="Upbeat Nutrition"></a>
      </a>
      <button id="navBtn" class="navbar-toggler hamburger hamburger--vortex" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>

      <div class="collapse navbar-collapse pt-4 mt-1" id="primaryNav">
        <ul class="navbar-nav ml-auto">
          @foreach ($primary_navigation as $item)
            <li class="px-4 h4 text-uppercase nav-item {{ $item->classes ?? '' }} {{ $item->active ? 'active' : '' }}">
              <a id="navLink{{ $item->id }}" class="nav-link" href="{{ $item->url }}">{{ $item->label }}</a>
            </li>
          @endforeach
        </ul>
      </div>
    </nav>
  </div>
</header>
